import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';



//material angular
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { NavComponent } from './nav/nav.component';
import { TodosComponent } from './todos/todos.component';
import { TodoComponent } from './todo/todo.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { CodesComponent } from './codes/codes.component';
import { Routes, RouterModule } from '@angular/router'; // פקודה להבאת הנתיבים
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
//firebase modules
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import{environment} from '../environments/environment';
import { UsertodosComponent } from './usertodos/usertodos.component';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NavComponent,
    TodosComponent,
    TodoComponent,
    RegistrationComponent,
    LoginComponent,
    CodesComponent,
    UsertodosComponent
  ],
  imports: [
    BrowserModule,
    MatCardModule,   // הוספנו
    MatButtonModule,
    MatInputModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    FormsModule,
    RouterModule.forRoot([ // כאן נגדיר נתיב, כל נתיב הינו ג'ייסון
    {path:'', component:TodosComponent}, //ברירת מחדל
    {path:'register', component:RegistrationComponent},
    {path:'login', component:LoginComponent},
    {path:'codes', component:CodesComponent},
    {path:'usertodos', component:UsertodosComponent},
    {path:'**', component:TodosComponent} // אם היוזר מכניס קישור לא מוכר, לכאן זה יגיע

    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }